# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory autocd extendedglob
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/knikiel/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# _______________________________________________
################################## Custom configs

# exports
export EDITOR="vim"
export PATH=$PATH;$HOME/.multirust/toolchains/nightly/cargo/bin

# aliases
alias ls="ls --color=always"
alias ll="ls -l"
alias la="ls -a"
alias lla="ls -la"
alias lal="lla"
alias grep="grep --color=always"
alias hist="history | grep"
alias term="gnome-terminal"
alias files="dolphin ."

# autocomplete from history
bindkey "^[[A" history-beginning-search-backward
bindkey "^[[B" history-beginning-search-forward

# prompt
autoload -U promptinit
promptinit
prompt fade magenta white cyan
export PROMPT="$PROMPT%# "
export RPROMPT="%F{cyan}%?"

#terminal title
case $TERM in xterm*)
    #precmd () {print -Pn "\e]0;%n@%m: %~\a"}
    precmd () {print -Pn "\e]0;%n: %2~\a"}
    ;;
esac

# plugin
#nur bei ArchLinux
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
