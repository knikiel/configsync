" Meine config
set nocompatible

" Mehr in der UI
set ruler
set number
set showcmd

" Search option
set incsearch
set smartcase
set ignorecase 
set hlsearch

" Syntax etc.
syntax on
filetype plugin indent on
set autoindent
set foldmethod=syntax

" Handling of tabs
set expandtab
set tabstop=4
set shiftwidth=4

"nicer escaping to normal mode
inoremap jk <ESC>
inoremap kj <ESC>

"clipboard copying and pasting
vnoremap <C-y> "+y
vnoremap <C-c> "+c<ESC> 
nnoremap <C-p> "+p
inoremap <C-p> <ESC>"+Pi

"find trailing whitespace
nnoremap <C-s> /\s\+$<RETURN>

"colors
colorscheme murphy 

"latex
let g:tex_flavor="latex"

"always show TODO/FIXME
augroup HighlightTODO
    autocmd!
    autocmd! WinEnter,VimEnter * :silent! call matchadd('Todo', 'TODO\|FIXME')
augroup END
