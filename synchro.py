#!/usr/bin/python3
# coding=utf-8

import argparse
import configparser
import getpass
import os
import os.path
import shutil
import socket

pygit2_loaded = True
try:
    import pygit2
except:
    pygit2_loaded = False
    print("'pygit2' not installed! You have to pull or push manually.")
    answer = input("Continue for simple copying?(y/N) ").strip()
    if answer != "y":
        print("Exiting...")
        exit()

### contents of config file:
# [DEFAULT]
# user=NAME
# password=*****
# repo_path=path/to/repo
# script_path=path/to/script
# [vim_path=path/to/.vimrc]
# [zsh_path=path/to/.zshrc]
# [git_path=path/to/.gitconfig]

USER = "user"
PASSWORD = "password"
REPO_PATH = "repo_path"
SCRIPT_PATH = "script_path"
VIM_PATH = "vim_path"
ZSH_PATH = "zsh_path"
GIT_PATH = "git_path"

def read_config():
    config = configparser.ConfigParser()
    config.read_file(open("synchro.cfg"))
    config = config["DEFAULT"]
    if (REPO_PATH not in config or SCRIPT_PATH not in config or
            USER not in config or PASSWORD not in config):
        print("Either {} or {} or {} or {} are missing in configuration file.".format(
                REPO_PATH, SCRIPT_PATH, USER, PASSWORD))
        exit(1)
    return config

def pull(args):
    print("Start of PULLing...")
    print("Reading config...")
    config = read_config()
    repo_path = config[REPO_PATH]

    if pygit2_loaded:
        repo = pygit2.Repository(os.path.join(repo_path, ".git"))
        repo.checkout_head()

        print("Pulling from remote...")
        remote = repo.remotes["origin"]
        userpass = pygit2.UserPass(config[USER], config[PASSWORD])
        remote.credentials = userpass
        remote.fetch()
        remote_master = repo.lookup_branch("origin/master", pygit2.GIT_BRANCH_REMOTE)
        master = repo.lookup_branch("master")
        repo.merge(remote_master.target)
        repo.checkout(master)
        master.set_target(remote_master.target)
        repo.head.set_target(remote_master.target)

    print("Installing 'synchro.py'...")
    script_path = config[SCRIPT_PATH]
    shutil.copy(os.path.join(repo_path, "synchro.py"), script_path)
    
    if VIM_PATH in config:
        print("Installing '.vimrc'...")
        vim_path = config[VIM_PATH]
        shutil.copy(os.path.join(repo_path, ".vimrc"), vim_path)

    if ZSH_PATH in config:
        print("Installing '.zshrc'...")
        zsh_path = config[ZSH_PATH]
        shutil.copy(os.path.join(repo_path, ".zshrc"), zsh_path)

    if GIT_PATH in config:
        print("Installing '.gitconfig'...")
        git_path = config[GIT_PATH]
        shutil.copy(os.path.join(repo_path, ".gitconfig"), git_path)

def push(args):
    print("Start of PUSHing...")
    print("Reading config...")
    config = read_config()
    repo_path = config[REPO_PATH]
    
    print("Preparing push of 'synchro.py'")
    script_path = config[SCRIPT_PATH]
    shutil.copy(script_path, os.path.join(repo_path, "synchro.py"))

    if VIM_PATH in config:
        print("Preparing push of '.vim'")
        vim_path = config[VIM_PATH]
        shutil.copy(vim_path, os.path.join(repo_path, ".vimrc"))

    if ZSH_PATH in config:
        print("Preparing push of '.zshrc'")
        zsh_path = config[ZSH_PATH]
        shutil.copy(zsh_path, os.path.join(repo_path, ".zshrc"))

    if GIT_PATH in config:
        print("Preparing push of '.gitconfig'...")
        git_path = config[GIT_PATH]
        shutil.copy(git_path, os.path.join(repo_path, ".gitconfig"))

    if pygit2_loaded:
        repo = pygit2.Repository(os.path.join(repo_path, ".git"))
        print("Merging...")
        os.chdir(repo_path)
        index = repo.index
        index.add_all()
        old_tree = repo.get(repo.head.target).tree
        tree_id = index.write_tree()
        diff = repo.diff(old_tree, repo.get(tree_id))
        modified_files = set()
        for patch in diff:
            modified_files.add(patch.old_file_path)
        if len(modified_files) == 0:
            print("Nothing changed. Aborting push...")
            return
        commit_author = "{}@{}".format(getpass.getuser(), socket.gethostname())
        commit_msg =  "{}: {}".format(commit_author, ", ".join(modified_files))
        print ("Commit message: '{}'".format(commit_msg))
        signature = repo.default_signature
        commit = repo.create_commit("refs/heads/master",
                signature, signature, commit_msg,
                tree_id, [repo.head.target])
        master = repo.lookup_branch("master")
        master.set_target(commit)
        repo.head.set_target(commit)
        repo.reset(commit, pygit2.GIT_RESET_HARD)

        print("Pushing to remote...")
        remote = repo.remotes["origin"]
        userpass = pygit2.UserPass(config[USER], config[PASSWORD])
        remote.credentials = userpass
        remote.push(["refs/heads/master"])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Keep config files updated.")
    subparsers = parser.add_subparsers()
    subparsers.required = True
    pull_parser = subparsers.add_parser("pull", description="Update local configs.")
    pull_parser.set_defaults(func = pull)
    push_parser = subparsers.add_parser("push", description="Push local changes to remote configs.")
    push_parser.set_defaults(func = push)

    args = parser.parse_args()
    args.func(args)
